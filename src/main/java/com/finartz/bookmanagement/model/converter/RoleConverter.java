package com.finartz.bookmanagement.model.converter;

import com.finartz.bookmanagement.model.domain.Role;
import com.finartz.bookmanagement.model.dto.RoleDto;
import org.springframework.stereotype.Component;

@Component
public class RoleConverter {

    public RoleDto convertToRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }
}
