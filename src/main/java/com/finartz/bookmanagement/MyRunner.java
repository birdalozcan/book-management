package com.finartz.bookmanagement;

import com.finartz.bookmanagement.model.domain.Role;
import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.service.RoleService;
import com.finartz.bookmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class MyRunner implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Override
    public void run(String... args) throws Exception {
        /*
        String password = "1234";
        String encode = new BCryptPasswordEncoder().encode(password);

        Role roleAdmin = roleService.findRoleAdmin();

        User user = new User();

        user.setName("Birdal");
        user.setSurname("Özcan");
        user.setUsername("birdalozcan");
        user.setPassword(encode);
        user.setRole(roleAdmin);
        user.setEnabled(true);
        user = userService.saveUserActive(user);
        System.out.println(user);

         */
    }
}
