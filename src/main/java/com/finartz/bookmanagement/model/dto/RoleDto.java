package com.finartz.bookmanagement.model.dto;

import lombok.Data;

@Data
public class RoleDto {
    private Long id;
    private String name;
}
