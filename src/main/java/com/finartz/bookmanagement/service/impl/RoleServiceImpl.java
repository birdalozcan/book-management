package com.finartz.bookmanagement.service.impl;

import com.finartz.bookmanagement.model.domain.Role;
import com.finartz.bookmanagement.model.repository.RoleRepository;
import com.finartz.bookmanagement.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findRoleAdmin() {
        Optional<Role> roleAdmin = roleRepository.findRoleAdmin();
        if (roleAdmin.isPresent()) {
            return roleAdmin.get();
        }
        return null;
    }

    @Override
    public Role findRoleUser() {
        Optional<Role> roleUser = roleRepository.findRoleUser();
        if (roleUser.isPresent()) {
            return roleUser.get();
        }
        return null;
    }
}
