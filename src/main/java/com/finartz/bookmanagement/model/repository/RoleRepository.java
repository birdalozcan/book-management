package com.finartz.bookmanagement.model.repository;

import com.finartz.bookmanagement.model.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("select role from Role role where role.name= 'ROLE_ADMIN'")
    Optional<Role> findRoleAdmin();

    @Query("select role from Role role where role.name= 'ROLE_USER'")
    Optional<Role> findRoleUser();
}
