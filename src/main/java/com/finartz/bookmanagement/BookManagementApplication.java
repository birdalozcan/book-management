package com.finartz.bookmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookManagementApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BookManagementApplication.class, args);
    }
}

