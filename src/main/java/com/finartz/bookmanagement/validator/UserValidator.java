package com.finartz.bookmanagement.validator;

import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.dto.UserDto;
import com.finartz.bookmanagement.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    private final int MIN_USERNAME_SIZE=3;
    private final int MAX_USERNAME_SIZE=10;
    private final int MIN_PASSWORD_SIZE=3;
    private final int MAX_PASSWORD_SIZE=10;

    private final UserService userService;

    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto user = (UserDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (user.getUsername().length() < MIN_USERNAME_SIZE || user.getUsername().length() > MAX_USERNAME_SIZE) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < MIN_PASSWORD_SIZE || user.getPassword().length() > MAX_PASSWORD_SIZE) {
            errors.rejectValue("password", "Size.userForm.password");
        }
    }
}
