<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Kitap Kayıt</title>

      <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">

        <form:form method="POST" action="${contextPath}/books/save-or-update" modelAttribute="bookForm" class="form-signin">
            <h2 class="form-signin-heading">Kitap Kaydet</h2>

            <spring:bind path="id">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:input type="hidden" path="id" class="form-control" placeholder="Id"
                                autofocus="true" value="${bookForm.id}"></form:input>
                    <form:errors path="id"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="name">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:input type="text" path="name" class="form-control" placeholder="Kitap Adı"
                                autofocus="true" value="${bookForm.name}"></form:input>
                    <form:errors path="name"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="author">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:input type="text" path="author" class="form-control" placeholder="Yazar"
                                autofocus="true" value="${bookForm.author}"></form:input>
                    <form:errors path="author"></form:errors>
                </div>
            </spring:bind>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Kaydet</button>
        </form:form>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>
