package com.finartz.bookmanagement.controller;

import com.finartz.bookmanagement.model.converter.UserConverter;
import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.dto.UserDto;
import com.finartz.bookmanagement.service.UserService;
import com.finartz.bookmanagement.validator.UserValidator;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
class UserController implements BaseController {

    private final UserService userService;
    private final UserValidator userValidator;
    private final UserConverter userConverter;

    private UserController(UserService userService, UserValidator userValidator, UserConverter userConverter) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.userConverter = userConverter;
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Kullanıcı adı ve şifre geçersiz.");
        }

        if (logout != null) {
            model.addAttribute("message", "Çıkış yapıldı.");
        }

        return "login";
    }

    @GetMapping({"/", "/welcome"})
    public String welcome() {
        return "welcome";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/users")
    public ModelAndView showUsers() {

        ModelAndView modelAndView = new ModelAndView("admin/user_show");

        List<User> userList = userService.findAllUserAndRoleUser();
        List<UserDto> userDtoList = userConverter.convertToUserDtoList(userList);

        modelAndView.addObject("userDtoList", userDtoList);

        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/users/save-or-update")
    public String showUsers(Model model) {

        model.addAttribute("userForm", new UserDto());

        return "admin/user_edit";
    }

    @PostMapping("/users/save-or-update")
    public String registration(@ModelAttribute("userForm") UserDto userDto, BindingResult bindingResult) {

        User userById = null;

        if (userDto.getId() == null) {
            userValidator.validate(userDto, bindingResult);
        } else {
            userById = userService.findById(userDto.getId());
        }

        if (bindingResult.hasErrors()) {
            return "admin/user_edit";
        }

        if (userById != null) {
            userService.saveWithoutRoleAndEnabled(userDto, userById);
        } else {
            User user = userConverter.convertToUser(userDto);
            userService.saveOrUpdate(user);
        }

        return "redirect:/users";
    }

    @PostMapping("/users/{id}/update")
    public String update(@PathVariable("id") Long userId, Model model) {

        User user = userService.findById(userId);

        UserDto userDto = userConverter.convertToUserDto(user);

        model.addAttribute("userForm", userDto);

        return "admin/user_edit";
    }

    @PostMapping("/users/{id}/delete")
    public String delete(@PathVariable("id") Long userId) {

        User user = userService.findById(userId);
        userService.delete(user);

        return "redirect:/users";
    }

    @PostMapping("/users/{id}/inactive")
    public String inactive(@PathVariable("id") Long userId) {

        User user = userService.findById(userId);
        userService.saveUserInActive(user);

        return "redirect:/users";
    }

    @PostMapping("/users/{id}/active")
    public String active(@PathVariable("id") Long userId) {

        User user = userService.findById(userId);
        userService.saveUserActive(user);

        return "redirect:/users";
    }
}
