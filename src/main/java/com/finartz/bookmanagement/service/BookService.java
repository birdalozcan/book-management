package com.finartz.bookmanagement.service;

import com.finartz.bookmanagement.model.domain.Book;
import com.finartz.bookmanagement.model.dto.BookDto;

import java.util.List;

public interface BookService {
    List<Book> findAll();
    void delete(Book book);
    Book findById(Long bookId);
    Book save(BookDto bookDto, Book bookById);
    Book save(Book book);
}
