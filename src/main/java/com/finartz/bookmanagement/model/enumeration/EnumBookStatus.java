package com.finartz.bookmanagement.model.enumeration;

public enum EnumBookStatus implements BaseEnum {
    OKUNMUS("Okunmuş"),
    OKUNACAK("Okunacak");

    private final String status;

    EnumBookStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
