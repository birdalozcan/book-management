package com.finartz.bookmanagement.model.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private boolean enabled;
    private RoleDto roleDto;
}
