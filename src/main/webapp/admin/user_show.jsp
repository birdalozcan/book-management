<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Kullanıcılar</title>

      <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
        <h4 class="text-center"><a href="${contextPath}/users/save-or-update">Kullanıcı Kaydet</a></h4>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>İsim</th>
                    <th>Soyisim</th>
                    <th>Kullanıcı Adı</th>
                    <th>Password</th>
                    <th>Düzenle</th>
                    <th>Sil</th>
                    <th>Aktif Et/Engelle</th>
                </tr>
            </thead>

            <c:forEach var="userDto" items="${userDtoList}">
                <tr>
                    <td>${userDto.id}</td>
                    <td>${userDto.name}</td>
                    <td>${userDto.surname}</td>
                    <td>${userDto.username}</td>
                    <td>${userDto.password}</td>
                    <td>
                        <form:form action="${contextPath}/users/${userDto.id}/update" method="post">
                            <button class="btn btn-info" type="submit">Düzenle</button>
                        </form:form>
                    </td>
                    <td>
                        <form:form action="${contextPath}/users/${userDto.id}/delete" method="post">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form:form>
                    </td>
                    <c:choose>
                        <c:when test="${userDto.enabled == true}">
                            <td>
                                <form:form action="${contextPath}/users/${userDto.id}/inactive" method="post">
                                    <button class="btn btn-success" type="submit">Aktif</button>
                                </form:form>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td>
                                <form:form action="${contextPath}/users/${userDto.id}/active" method="post">
                                    <button class="btn btn-warning" type="submit">Engellendi</button>
                                </form:form>
                            </td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>
