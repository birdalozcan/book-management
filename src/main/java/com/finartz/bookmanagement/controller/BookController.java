package com.finartz.bookmanagement.controller;

import com.finartz.bookmanagement.model.converter.BookConverter;
import com.finartz.bookmanagement.validator.BookValidator;
import com.finartz.bookmanagement.model.domain.Book;
import com.finartz.bookmanagement.model.dto.BookDto;
import com.finartz.bookmanagement.service.BookService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
class BookController implements BaseController {

    private final BookService bookService;
    private final BookConverter bookConverter;
    private final BookValidator bookValidator;

    private BookController(BookService bookService, BookConverter bookConverter, BookValidator bookValidator) {
        this.bookService = bookService;
        this.bookConverter = bookConverter;
        this.bookValidator = bookValidator;
    }

    @GetMapping("/books")
    public ModelAndView showBooks() {

        ModelAndView modelAndView = new ModelAndView("admin/book_show");

        List<Book> bookList = bookService.findAll();
        List<BookDto> bookDtoList = bookConverter.convertToBookDtoList(bookList);

        modelAndView.addObject("bookDtoList", bookDtoList);

        return modelAndView;
    }

    @PostMapping("/books/{id}/update")
    public String update(@PathVariable("id") Long bookId, Model model) {

        Book book = bookService.findById(bookId);

        BookDto bookDto = bookConverter.convertToBookDto(book);

        model.addAttribute("bookForm", bookDto);

        return "admin/book_edit";
    }

    @PostMapping("/books/{id}/delete")
    public String delete(@PathVariable("id") Long bookId) {

        Book book = bookService.findById(bookId);
        bookService.delete(book);

        return "redirect:/books";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/books/save-or-update")
    public String showbooks(Model model) {

        model.addAttribute("bookForm", new BookDto());

        return "admin/book_edit";
    }

    @PostMapping("/books/save-or-update")
    public String save(@ModelAttribute("bookForm") BookDto bookDto, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "admin/book_edit";
        }

        Book bookById = null;

        if (bookDto.getId() == null) {
            bookValidator.validate(bookDto, bindingResult);
        } else {
            bookById = bookService.findById(bookDto.getId());
        }

        if (bookById != null) {
            bookService.save(bookDto, bookById);
        } else {
            Book book = bookConverter.convertToBook(bookDto);
            bookService.save(book);
        }

        return "redirect:/books";
    }
}
