package com.finartz.bookmanagement.model.repository;

import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select mem from User mem where mem.username = :username")
    Optional<User> findByUsername(@Param("username")String username);

    @Query("select mem from User mem where mem.role = :roleUser")
    Optional<List<User>> findAllUserAndRoleUser(@Param("roleUser")Role roleUser);
}
