package com.finartz.bookmanagement.model.domain;

import com.finartz.bookmanagement.model.enumeration.EnumBookStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USER_BOOK")
public class UserBook extends BaseEntity {

    @Id
    @SequenceGenerator(name = "generator", allocationSize = 1, sequenceName = "USER_BOOK_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
    @Column
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_USER")
    @org.hibernate.annotations.ForeignKey(name = "FK_USER_BOOK_USER")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_BOOK")
    @org.hibernate.annotations.ForeignKey(name = "FK_USER_BOOK_BOOK")
    private Book book;

    @Enumerated(EnumType.STRING)
    @Column
    private EnumBookStatus bookStatus;
}
