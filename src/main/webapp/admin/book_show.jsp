<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Kullanıcılar</title>

      <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
        <h4 class="text-center"><a href="${contextPath}/books/save-or-update">Kitap Kaydet</a></h4>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Kitap Adı</th>
                    <th>Yazar</th>
                    <th>Düzenle</th>
                    <th>Sil</th>
                </tr>
            </thead>

            <c:forEach var="bookDto" items="${bookDtoList}">
                <tr>
                    <td>${bookDto.id}</td>
                    <td>${bookDto.name}</td>
                    <td>${bookDto.author}</td>
                    <td>
                        <form:form action="${contextPath}/books/${bookDto.id}/update" method="post">
                            <button class="btn btn-info" type="submit">Düzenle</button>
                        </form:form>
                    </td>
                    <td>
                        <form:form action="${contextPath}/books/${bookDto.id}/delete" method="post">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  </body>
</html>
