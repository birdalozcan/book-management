package com.finartz.bookmanagement.service.impl;

import com.finartz.bookmanagement.model.domain.Book;
import com.finartz.bookmanagement.model.dto.BookDto;
import com.finartz.bookmanagement.model.repository.BookRepository;
import com.finartz.bookmanagement.service.BookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> findAll() {
       return bookRepository.findAll();
    }

    @Override
    public void delete(Book book) {
        bookRepository.delete(book);
    }

    @Override
    public Book findById(Long bookId) {
        Optional<Book> bookOptional = bookRepository.findById(bookId);
        return bookOptional.orElse(null);
    }

    @Override
    public Book save(BookDto bookDto, Book bookById) {
        bookById.setName(bookDto.getName());
        bookById.setAuthor(bookDto.getAuthor());
        return bookRepository.save(bookById);
    }

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }
}
