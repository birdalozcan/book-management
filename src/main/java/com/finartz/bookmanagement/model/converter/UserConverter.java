package com.finartz.bookmanagement.model.converter;

import com.finartz.bookmanagement.model.domain.Role;
import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.dto.RoleDto;
import com.finartz.bookmanagement.model.dto.UserDto;
import com.finartz.bookmanagement.service.RoleService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserConverter {

    private final RoleService roleService;
    private final RoleConverter roleConverter;

    public UserConverter(RoleService roleService, RoleConverter roleConverter) {
        this.roleService = roleService;
        this.roleConverter = roleConverter;
    }

    public UserDto convertToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());

        RoleDto roleDto = roleConverter.convertToRoleDto(user.getRole());
        userDto.setRoleDto(roleDto);
        userDto.setEnabled(user.isEnabled());
        return userDto;
    }

    public List<UserDto> convertToUserDtoList(List<User> userList) {
        List<UserDto> userDtoList = new ArrayList<>();

        if (userList != null && !userList.isEmpty()) {
            for (User user : userList) {
                userDtoList.add(convertToUserDto(user));
            }
        }

        return userDtoList;
    }

    public User convertToUser(UserDto userDto) {

        Role roleUser = roleService.findRoleUser();

        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setRole(roleUser);
        user.setEnabled(true);

        return user;
    }
}
