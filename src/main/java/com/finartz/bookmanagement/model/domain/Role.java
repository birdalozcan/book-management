package com.finartz.bookmanagement.model.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ROLE")
public class Role extends BaseEntity {

    @Id
    @SequenceGenerator(name = "generator", allocationSize = 1, sequenceName = "USER_ROLE_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
    @Column
    private Long id;

    @Column
    private String name;
}
