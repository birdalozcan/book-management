package com.finartz.bookmanagement.model.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "USER")
public class User extends BaseEntity {

    @Id
    @SequenceGenerator(name = "generator", allocationSize = 1, sequenceName = "USER_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private boolean enabled;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_ROLE")
    @org.hibernate.annotations.ForeignKey(name = "FK_USER_ROLE")
    private Role role;
}
