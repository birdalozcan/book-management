package com.finartz.bookmanagement.service.impl;

import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.domain.Role;
import com.finartz.bookmanagement.model.dto.UserDto;
import com.finartz.bookmanagement.model.repository.UserRepository;
import com.finartz.bookmanagement.service.UserService;
import com.finartz.bookmanagement.service.RoleService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;

    public UserServiceImpl(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        User user = findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getName()));

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), grantedAuthorities);
    }

    @Override
    public User saveOrUpdate(User user) {
        userSetRoleAndEnabled(user, true);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User saveUserActive(User user) {

        userSetRoleAndEnabled(user, true);

        if (user.getId() == null) {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        }

        return userRepository.save(user);
    }

    @Override
    public User saveUserInActive(User user) {
        userSetRoleAndEnabled(user, false);
        return userRepository.save(user);
    }

    public void userSetRoleAndEnabled(User user, boolean enabled) {
        Role roleUser = roleService.findRoleUser();
        user.setRole(roleUser);
        user.setEnabled(enabled);
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isPresent()) {
            return userOptional.get();
        }

        return null;
    }

    @Override
    public List<User> findAllUserAndRoleUser() {
        Role roleUser = roleService.findRoleUser();
        Optional<List<User>> allUserAndRoleUser = userRepository.findAllUserAndRoleUser(roleUser);
        if (allUserAndRoleUser.isPresent()) {
            return allUserAndRoleUser.get();
        }

        return null;
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findById(Long memberId) {
        Optional<User> userOptional = userRepository.findById(memberId);
        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        return null;
    }

    @Override
    public User saveWithoutRoleAndEnabled(UserDto userDto, User user) {
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setUsername(userDto.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
        return userRepository.save(user);
    }
}
