package com.finartz.bookmanagement.service;

import com.finartz.bookmanagement.model.domain.User;
import com.finartz.bookmanagement.model.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    User saveOrUpdate(User user);
    User findByUsername(String username);
    List<User> findAllUserAndRoleUser();
    void delete(User user);
    User findById(Long memberId);
    User saveWithoutRoleAndEnabled(UserDto userDto, User user);
    User saveUserActive(User user);
    User saveUserInActive(User user);
}
