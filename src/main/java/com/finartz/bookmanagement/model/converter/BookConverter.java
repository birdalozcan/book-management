package com.finartz.bookmanagement.model.converter;

import com.finartz.bookmanagement.model.domain.Book;
import com.finartz.bookmanagement.model.dto.BookDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookConverter {

    public BookDto convertToBookDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setAuthor(book.getAuthor());
        return bookDto;
    }

    public List<BookDto> convertToBookDtoList(List<Book> bookList) {
        List<BookDto> bookDtoList = new ArrayList<>();

        if (bookList != null && !bookList.isEmpty()) {
            for (Book book : bookList) {
                bookDtoList.add(convertToBookDto(book));
            }
        }

        return bookDtoList;
    }

    public Book convertToBook(BookDto bookDto) {
        Book book = new Book();
        book.setId(bookDto.getId());
        book.setName(bookDto.getName());
        book.setAuthor(bookDto.getAuthor());
        return book;
    }
}
