package com.finartz.bookmanagement.model.dto;

import lombok.Data;

@Data
public class BookDto {
    private Long id;
    private String name;
    private String author;
}
