package com.finartz.bookmanagement.model.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "BOOK")
public class Book extends BaseEntity{

    @Id
    @SequenceGenerator(name = "generator", allocationSize = 1, sequenceName = "BOOK_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private String author;
}
