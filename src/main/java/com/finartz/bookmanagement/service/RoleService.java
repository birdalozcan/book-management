package com.finartz.bookmanagement.service;

import com.finartz.bookmanagement.model.domain.Role;

public interface RoleService {

    Role findRoleAdmin();
    Role findRoleUser();
}
